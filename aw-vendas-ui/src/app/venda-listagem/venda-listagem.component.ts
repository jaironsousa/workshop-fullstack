import { Component, OnInit } from '@angular/core';
import { VendasService } from '../vendas/vendas.service';

@Component({
  selector: 'app-venda-listagem',
  templateUrl: './venda-listagem.component.html',
  styleUrls: ['./venda-listagem.component.css']
})
export class VendaListagemComponent implements OnInit {

  vendas: Array<any>;

  constructor(
    private vendasService: VendasService
  ) { }

  ngOnInit() {
    this.listar();
  }

  listar() {
    this.vendasService.listar()
      .subscribe(response => this.vendas = response)
  }

}
