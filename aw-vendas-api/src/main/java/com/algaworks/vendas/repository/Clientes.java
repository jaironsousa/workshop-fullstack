package com.algaworks.vendas.repository;

import com.algaworks.vendas.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Clientes extends JpaRepository<Cliente, Long> {
}
