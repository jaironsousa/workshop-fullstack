package com.algaworks.vendas.repository;

import com.algaworks.vendas.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Produtos extends JpaRepository<Produto, Long> {
}
