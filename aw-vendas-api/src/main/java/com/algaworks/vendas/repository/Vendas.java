package com.algaworks.vendas.repository;

import com.algaworks.vendas.model.Venda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Vendas extends JpaRepository<Venda, Long> {
}
