package com.algaworks.vendas.service;

import com.algaworks.vendas.model.Venda;
import com.algaworks.vendas.repository.Produtos;
import com.algaworks.vendas.repository.Vendas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class VendaService {

    @Autowired
    private Vendas vendas;

    @Autowired
    private Produtos produtos;


    public Venda adicionar(Venda venda) {
        venda.setCadastro(LocalDateTime.now());
        venda.getItens().forEach(i -> {
            i.setVenda(venda);
            i.setProduto(produtos.findById(i.getProduto().getId()).get());
        });

        // Metodo para calcular o total dos pedidos
        BigDecimal totslItens = venda.getItens().stream()
                .map(i -> i.getProduto().getValor().multiply(new BigDecimal(i.getQuantidade())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        venda.setTotal(totslItens.add(venda.getFrete()));

        return vendas.save(venda);
    }

    public List<Venda> buscarTodas() {
        return vendas.findAll();
    }

}
