package com.algaworks.vendas.resource;

import com.algaworks.vendas.model.Cliente;
import com.algaworks.vendas.model.Venda;
import com.algaworks.vendas.repository.Clientes;
import com.algaworks.vendas.repository.Vendas;
import com.algaworks.vendas.service.VendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/vendas")
public class VendasResource {

    @Autowired
    private VendaService vendaService;

    @GetMapping
    public List<Venda> listar() {
        return vendaService.buscarTodas();
    }

    @PostMapping
    public Venda adicionar(@RequestBody @Valid Venda venda) {
        return vendaService.adicionar(venda);
    }
}


